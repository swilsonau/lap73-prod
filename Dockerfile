FROM centos:7

# Installing Epel and Remi repos for PHP 7.3
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm https://rpms.remirepo.net/enterprise/remi-release-7.rpm https://centos7.iuscommunity.org/ius-release.rpm

# Installing PHP, Apache and MariaDB
RUN yum -y --enablerepo=remi,remi-php73 install httpd php php-common php-cli php-mbstring php-mcrypt php-xml php-mysql php-intl php-ldap sudo php-gd php-pecl-zip php-libxml php-openssl php-zlib

# Install Other Required Packages
RUN yum -y install wget zip unzip fontconfig libXrender libXext xorg-x11-fonts-Type1 xorg-x11-fonts-75dpi freetype lib icu openssl libpng libjpeg-turbo qt5-qtwebkit qt5-qtsvg qt5-qtxmlpatterns

# Clean up
RUN yum clean all &&  rm -rf /var/cache/yum

# Install WKHTMLTOX
ADD wkhtmltox/wkhtmltopdf /usr/bin/wkhtmltopdf
ADD wkhtmltox/wkhtmltoimage /usr/bin/wkhtmltoimage
ADD wkhtmltox/libwkhtmltox* /usr/local/lib/

# Apache Setup
RUN mkdir /etc/httpd/conf/sites-enabled && echo "IncludeOptional conf/sites-enabled/*.conf" >> /etc/httpd/conf/httpd.conf
ADD apache_default /etc/httpd/conf/sites-enabled/000-default.conf

# PHP Ini Setup
RUN sed -i -e 's~^;date.timezone =$~date.timezone = Australia/Sydney~g' /etc/php.ini && sed -i -e 's~^max_execution_time = 30$~max_execution_time = 0~g' /etc/php.ini && sed -i -e 's~^upload_max_filesize = 2M$~upload_max_filesize = 50M~g' /etc/php.ini && sed -i -e 's~^post_max_size = 8M$~post_max_size = 50M~g' /etc/php.ini && sed -i -e 's~^memory_limit = 128M$~memory_limit = 1024M~g' /etc/php.ini

EXPOSE 80

ENV LD_LIBRARY_PATH=/usr/local/lib
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
